import Vue from 'vue';
import Router from 'vue-router';

export default new Router({
  mode: 'history',
  scrollBehavior: () => ({
    y: 0,
  }),
  routes: [],
});
