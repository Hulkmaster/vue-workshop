/**
 * @typedef {Object} Apartment
 * @property {string} id
 * @property {string} name
 * @property {Object} size
 * @property {number} size.width
 * @property {number} size.depth
 * @property {Room[]} rooms
 */

/**
 * @typedef {Object} Room
 * @property {string} id
 * @property {string} name
 * @property {string} apartmentId
 * @property {Position} position
 * @property {Furniture[]} furniture
 */

/**
 * @typedef {Object} Furniture
 * @property {string} id
 * @property {string} name
 * @property {string} roomId
 * @property {Position} position
 */

/**
 * @typedef {Object} Coordinate
 * @property {number} x
 * @property {number} y
 */

/**
 * @typedef {Object} Position
 * @property {Coordinate} start
 * @property {Coordinate} end
 */
