function filterMockupData({ id, parentIdPropertyName, data, multiple = false }) {
  let returnData = multiple ? [] : {};

  for (let item of data) {
    if (item[parentIdPropertyName] === id) {
      if (multiple) {
        returnData.push(item);
      } else {
        returnData = item;
        break;
      }
    }
  }

  return returnData;
}

module.exports = {
  filterMockupData,
};
