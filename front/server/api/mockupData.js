const { Apartment, Room, Furniture } = require('./Types.js');

/** @type {Apartment[]} */
const apartaments = [
  {
    id: '0',
    name: 'First',
    size: {
      width: 15,
      depth: 15,
    },
  },
  {
    id: '1',
    name: 'Second',
    size: {
      width: 30,
      depth: 15,
    },
  },
  {
    id: '2',
    name: 'Third',
    size: {
      width: 15,
      depth: 30,
    },
  },
];

/** @type {Room[]} */
const rooms = [
  {
    id: '0',
    name: 'First Room',
    apartamentId: '0',
    position: {
      start: {
        x: 0,
        y: 0,
      },
      end: {
        x: 2,
        y: 2,
      },
    },
  },
  {
    id: '1',
    name: 'Second Room',
    apartamentId: '0',
    position: {
      start: {
        x: 0,
        y: 3,
      },
      end: {
        x: 0,
        y: 5,
      },
    },
  },
  {
    id: '2',
    name: 'Third Room',
    apartamentId: '0',
    position: {
      start: {
        x: 4,
        y: 4,
      },
      end: {
        x: 7,
        y: 8,
      },
    },
  },
];

/** @type {Furniture[]} */
const furniture = [
  {
    id: '0',
    name: 'Couch',
    roomId: '0',
    position: {
      start: {
        x: 0,
        y: 0,
      },
      end: {
        x: 0,
        y: 1,
      },
    },
  },
  {
    id: '1',
    name: 'Chair',
    roomId: '0',
    position: {
      start: {
        x: 1,
        y: 1,
      },
      end: {
        x: 1,
        y: 1,
      },
    },
  },
  {
    id: '2',
    name: 'Table',
    roomId: '0',
    position: {
      start: {
        x: 0,
        y: 1,
      },
      end: {
        x: 0,
        y: 1,
      },
    },
  },
];

module.exports = {
  apartaments,
  rooms,
  furniture,
};
