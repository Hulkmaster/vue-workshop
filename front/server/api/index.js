const router = require('express').Router();
const { filterMockupData } = require('./Utils');
const { apartaments, rooms, furniture } = require('./mockupData');

router.get('/apartament', function(req, res) {
  const apartamentsData = apartaments;

  for (let apartament of apartamentsData) {
    apartament.rooms = filterMockupData({
      id: apartament.id,
      parentIdPropertyName: 'apartamentId',
      data: rooms,
      multiple: true,
    });

    for (let room of apartament.rooms) {
      room.furniture = filterMockupData({
        id: room.id,
        parentIdPropertyName: 'roomId',
        data: furniture,
        multiple: true,
      });
    }
  }

  res.send(apartamentsData);
});

module.exports = router;
