const express = require('express');
const app = express();

const webpack = require('webpack');
const path = require('path');

const api = require('./api');

const clientConfig = require('../webpack.config.dev.js');
const clientCompiler = webpack(clientConfig);

const devMiddleware = require('webpack-dev-middleware')(clientCompiler, {
  publicPath: clientConfig.output.publicPath,
});

const hotMiddleware = require('webpack-hot-middleware')(clientCompiler, {
  heartbeat: 5000,
});

app.use('/api', api);

app.use(devMiddleware);
app.use(hotMiddleware);

app.get('*', (req, res) => {
  res.end(
    Buffer.from(devMiddleware.fileSystem.readFileSync(path.join(clientConfig.output.path, 'index.html'))),
    'binary'
  );
});

const port = process.env.PORT || 3000;

console.log(`server started on ${port}`);
app.listen(port);
