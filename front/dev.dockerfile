FROM node:10

RUN mkdir /usr/front
WORKDIR /usr/front
COPY package.json package-lock.json ./
RUN npm i
ENTRYPOINT npm run dev